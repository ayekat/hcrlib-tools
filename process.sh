#!/bin/sh --

# Dependencies:
# * coreutils
# * ex (vi)
# * sh (POSIX)

set -eu

readonly E_SUCCESS=0
readonly E_USER=1
readonly E_FILESYSTEM=2
readonly E_FORMAT=3
readonly E_INTERNAL=13

check_directories()
{
	echo 'Checking stage directories'
	if $diff; then
		set -- "$dir_old_sorted" "$dir_new_sorted"
	else
		set -- "$dir_original"
	fi
	for dir in "$@"; do
		test -d "$dir" || die $E_FILESYSTEM '%s: No such directory' "$dir"
	done
}

setup_directories()
{
	echo 'Setting up stage directories'
	set -- "$dir_sorted" "$dir_deduplicated"
	if ! $diff; then
		set -- "$dir_sanitised" "$dir_uniformised" "$@"
	fi
	for dir in "$@"; do
		debug '%s' "$dir"
		if $force; then
			rm -rf "$dir"
		elif [ -d "$dir" ]; then
			die $E_FILESYSTEM \
				'%s: Directory already exists; pass -f to overwrite' "$dir"
		fi
		mkdir "$dir"
	done
}

stage_uniformise_pubmed()
{
	_parse_record()
	{
		record_no=${record%%:*}
		record=${record#*: }
		test -n "$record_no" || die $E_FORMAT 'Empty record number'
		case $record_no in (*[!0-9]*)
			die $E_FORMAT '%s: Non-numeric record number' "$record_no"
		esac
		test -n "$record" || die $E_FORMAT 'Empty record'
	}

	write_record()
	{
		printf '%s\n' "$record"
	}

	reset_record()
	{
		record_no=''
		record=''
	}

	parse_line()
	{
		if [ -n "$line" ]; then
			if [ -z "$record" ]; then
				# new entry; strip record number
				record="$line"
				_parse_record
			else
				# append to existing entry
				record="$record $line"
			fi
		else
			# Empty line, no record (write out any active record):
			if [ -n "$record" ]; then
				write_record
				reset_record
			fi
		fi
	}
}

stage_uniformise_clinicaltrial()
{
	write_record()
	{
		test -n "$record_no" || die $E_FORMAT 'Missing number'
		test -n "$record_title" || die $E_FORMAT 'Missing title'
		printf '%d %s [%s]\n' "$record_no" "$record_title" "$record_url"
	}

	reset_record()
	{
		record_no=
		record_title=
		record_url=
	}

	parse_line()
	{
		if [ -n "$line" ]; then
			case $line in
				('Study List:')
					# Ignore: only printed at the beginning of a file
					;;
				('Study '*':')
					test -z "$record_no" \
						|| die $E_FORMAT 'Beginning of record inside record'
					record_no=${line#Study }
					record_no=${record_no%:}
					case $record_no in (*[!0-9]*)
						die $E_FORMAT 'Non-numeric record number: %s' \
						              "$record_no"
					esac ;;
				('Title: '*)
					test -n "$record_no" \
						|| die $E_FORMAT 'Title attribute outside of record'
					test -z "$record_title" \
						|| die $E_FORMAT 'Multiple Title attributes'
					# XXX This if super fragile, make this more robust:
					record_title=${line#Title:                        } ;;
				('URL: '*)
					test -n "$record_no" \
						|| die $E_FORMAT 'URL attribute outside of record'
					test -z "$record_url" \
						|| die $E_FORMAT 'Multiple URL attributes'
					# XXX This if super fragile, make this more robust:
					record_url=${line#URL:                          } ;;
				(*)
					;;
			esac
		else
			# Empty line, no record (write out any active record):
			if [ -n "$record_no" ]; then
				write_record
				reset_record
			fi
		fi
	}
}

stage_uniformise_cochrane()
{
	write_record()
	{
		test -n "$record_id" || die $E_FORMAT 'Missing ID'
		test -n "$record_title" || die $E_FORMAT 'Missing title'
		printf '%s [ID:%s]' "$record_title" "$record_id"
		if [ -n "$record_pmid" ]; then
			printf ' [%s]' "$record_pmid"
		fi
		echo
	}

	reset_record()
	{
		record_no=
		record_id=
		record_title=
		record_pmid=
	}

	parse_line()
	{
		if [ -n "$line" ]; then
			case $line in
				('Record #'*)
					test -z "$record_no" \
						|| die $E_FORMAT 'Beginning of record inside record'
					record_no=${line#Record #}
					record_no=${record_no% of *}
					case $record_no in (*[!0-9]*)
						die $E_FORMAT 'Non-numeric record number: %s' \
						              "$record_no"
					esac ;;
				('ID: '*)
					test -n "$record_no" \
						|| die $E_FORMAT 'ID attribute outside of record' \
					test -z "$record_id" \
						|| die $E_FORMAT 'Multiple ID attributes'
					record_id=${line#ID: } ;;
				('TI: '*)
					test -n "$record_no" \
						|| die $E_FORMAT 'TI attribute outside of record'
					test -z "$record_title" \
						|| die $E_FORMAT 'Multiple TI attributes'
					record_title=${line#TI: } ;;
				('PM: '*)
					test -n "$record_no" \
						|| die $E_FORMAT 'PM attribute outside of record'
					test -z "$record_pmid" \
						|| die $E_FORMAT 'Multiple PM attributes'
					record_pmid=${line#PM: } ;;
				(*) ;;
			esac
		else
			# Empty line, no record (write out any active record):
			if [ -n "$record_no" ]; then
				write_record
				reset_record
			fi
		fi
	}
}

stage_sanitise()
(
	set -eu

	echo 'Sanitising entries'
	for infile in "$dir_original"/*.txt; do
		outfile=$dir_sanitised/$(basename "$infile")
		debug '%s -> %s' "$infile" "$outfile"

		cp "$infile" "$outfile"
		printf 'wq\n' | ex "$outfile" >/dev/null # add EOL to last line
		dos2unix -q "$outfile" >&2
	done
)

stage_uniformise()
(
	set -eu

	echo 'Uniformising entries'
	stage_uniformise_$type

	for infile in "$dir_sanitised"/*.txt; do
		outfile=$dir_uniformised/$(basename "$infile")
		debug '%s -> %s' "$infile" "$outfile"

		reset_record
		lineno=0
		while read -r line; do
			lineno=$((lineno + 1))
			parse_line
		done <"$infile" >"$outfile"

		if [ -n "$record_no" ]; then
			write_record >>"$outfile"
			reset_record
		fi
	done
)

stage_sort()
{
	echo 'Sorting entries'
	for infile in "$dir_uniformised"/*.txt; do
		outfile="$dir_sorted/$(basename "$infile")"
		debug '%s -> %s' "$infile" "$outfile"
		<"$infile" sort >"$outfile"
	done
}

stage_deduplicate()
{
	largest()
	{
		maxlines=0
		maxfile=
		for f in "$@"; do
			test -e "$f" || die $E_INTERNAL 'called largest() on no files'
			nlines="$(wc -l "$f" | cut -d' ' -f1)"
			if [ $nlines -gt $maxlines ]; then
				maxlines=$nlines
				maxfile="$f"
			fi
		done
		echo "$maxfile"
	}

	set --
	for f in "$dir_sorted"/*.txt; do
		test -e "$f" || return # no files, nothing to do
		set -- "$@" "$f"
	done

	echo 'Deduplicating entries'
	while [ $# -gt 0 ]; do
		# Choose next-largest file among remaining ones:
		infile="$(largest "$@")"
		test -n "$infile"
		outfile="$dir_deduplicated/$(basename "$infile")"
		outfile_tmp="${outfile}_tmp"

		# Copy into target directory:
		debug '%s -> %s' "$infile" "$outfile"
		cp "$infile" "$outfile"

		# Deduplicate entries against all other files in target directory:
		for reffile in "$dir_deduplicated"/*.txt; do
			test "$reffile" != "$outfile" || continue
			comm -13 "$reffile" "$outfile" >"$outfile_tmp"
			mv "$outfile_tmp" "$outfile"
		done

		# Remove this file from the list of remaining files:
		for f in "$@"; do
			shift
			if [ "$f" = "$infile" ]; then
				break
			else
				set -- "$@" "$f"
			fi
		done
	done
}

stage_diff()
{
	for f_new in "$dir_new_sorted"/*.txt; do
		f_new=$(basename "$f_new")
		f=${f_new#*_}
		for f_old in "$dir_old_sorted"/*.txt ''; do
			f_old=$(basename "$f_old")
			test "${f_old#*_}" != "$f" || break
		done

		if [ -n "$f_old" ]; then
			comm -13 "$dir_old_sorted/$f_old" "$dir_new_sorted/$f_new" \
			         >"$dir_sorted/$f"
		else
			cp "$dir_new_sorted/$f_new" "$dir_sorted/$f"
		fi
	done
}

die()
{
	retval=$(($1 + 0)); shift
	if [ $# -gt 0 ]; then
		printf '\033[31m'
		if [ $retval -eq $E_FORMAT ]; then
			printf '%s:%d' "$infile" $lineno
			test -z "$record_no" || printf ' (Record #%s)' "$record_no"
		elif [ $retval -eq $E_INTERNAL ]; then
			printf 'E_INTERNAL'
		else
			printf 'ERROR'
		fi
		printf '\033[0m: '
		printf "$@"
		echo
	else
		cat
	fi >&2
	if [ $retval -eq $E_USER ]; then
		printf 'Run with `-h` for more usage information\n' >&2
	fi
	exit $retval
}

print_usage()
{
	cat <<- EOF
	$0: Process reference files from various sources

	Usage: $0 [OPTIONS] {type} {directory}
	       $0 [OPTIONS] diff {dir_old} {dir_new} {dir_out}

	Options:
	  -f     Overwrite existing sub-directories
	  -h     Display this message and exit

	Types:
	  pubmed
	  cochrane
	  clinicaltrial

	Directory:
	  Must contain a sub-directory named \`original\`, containing the entries in
	  a form as downloaded from the corresponding source.

	This script will create four new sub-directories in the given {directory},
	where the entries will be
	  \`sanitised\`:    rewritten with some basic sanitising (EOL at EOF);
	  \`uniformised\`:  rewritten in a uniform (one-line) format;
	  \`sorted\`:       sorted alphabetically;
	  \`deduplicated\`: deduplicated (for multiple files).

	For the diff mode, files are expected to be named {date}_{search term}.txt
	The date format can be any one chosen (the content will be ignored); it must
	simply not contain any underscore characters, as that will be treated as a
	separator.
	EOF
}

debug()
{
	printf '\033[34m' >&2
	if [ $# -gt 0 ]; then
		printf "$@" >&2
		echo >&2
	else
		cat >&2
	fi
	printf '\033[0m' >&2
}

# options:
force=false
while getopts :fh opt; do
	case "$opt" in
		(f) force=true ;;
		(h) print_usage; exit $E_SUCCESS ;;
		(:) die $E_USER 'Missing argument for -%s' "$OPTARG" ;;
		('?') die $E_USER 'Unknown option -%s' "$OPTARG" ;;
		(*) die $E_INTERNAL 'Unhandled option -%s' "$OPTARG" ;;
	esac
done
shift $((OPTIND - 1))
unset OPTARG

# type:
diff=false
test $# -gt 0 || die $E_USER 'Missing source type'
readonly type="$1"; shift
case "$type" in
	(clinicaltrial|cochrane|pubmed) ;;
	(diff) diff=true ;;
	(*) die $E_USER 'Unknown source type: %s' "$type"
esac

# input directories:
if $diff; then
	test $# -gt 0 || die $E_USER 'Missing input directory 1 (old)'
	dir="$1"; shift
	readonly dir_old_sorted="$dir/sorted"

	test $# -gt 0 || die $E_USER 'Missing input directory 2 (new)'
	dir="$1"; shift
	readonly dir_new_sorted="$dir/sorted"
fi

# (output) directory:
test $# -gt 0 || die $E_USER 'Missing directory'
dir="$1"; shift
readonly dir_original="$dir/original"
readonly dir_sanitised="$dir/sanitised"
readonly dir_uniformised="$dir/uniformised"
readonly dir_sorted="$dir/sorted"
readonly dir_deduplicated="$dir/deduplicated"

debug << EOF
dir_original=$dir_original
dir_sanitised=$dir_sanitised
dir_uniformised=$dir_uniformised
dir_sorted=$dir_sorted
dir_deduplicated=$dir_deduplicated
EOF

check_directories
setup_directories
if ! $diff; then
	stage_sanitise
	stage_uniformise
	stage_sort
else
	stage_diff
fi
stage_deduplicate
