hcrlib-tools
============

This repository contains a small set of tools used for processing data obtained
from healthcare review libraries (Clinical Trial, Cochrane, Pubmed).

```
./process.sh: Process reference files from various sources

Usage: ./process.sh [OPTIONS] {type} {directory}
       ./process.sh [OPTIONS] diff {dir_old} {dir_new} {dir_out}

Options:
  -f     Overwrite existing sub-directories
  -h     Display this message and exit

Types:
  pubmed
  cochrane
  clinicaltrial

Directory:
  Must contain a sub-directory named `original`, containing the entries in
  a form as downloaded from the corresponding source.

This script will create four new sub-directories in the given {directory},
where the entries will be
  `sanitised`:    rewritten with some basic sanitising (EOL at EOF);
  `uniformised`:  rewritten in a uniform (one-line) format;
  `sorted`:       sorted alphabetically;
  `deduplicated`: deduplicated (for multiple files).

For the diff mode, files are expected to be named {date}_{search term}.txt
The date format can be any one chosen (the content will be ignored); it must
simply not contain any underscore characters, as that will be treated as a
separator.
```


### Random sample

For picking a random sample (e.g. 118 entries) of a file:

	$ shuf infile | head -n 118 | sort >outfile
